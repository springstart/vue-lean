// 定义常量(存储器的 key)
const STORAGE_KEY='todo_list';

// 开放出其他的两个方法
export default{
    // 清除数据
    fetch(){
        return JSON.parse(window.localStorage.getItem(STORAGE_KEY)||'[]')
    },
    // 保存数据
    save(items){
        window.localStorage.setItem(STORAGE_KEY,JSON.stringify(items))
    }
}
