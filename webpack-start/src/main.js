// 导入 vue 的 js 文件(引用的是 node_modules 下的 vue 文件)
import Vue from 'vue/dist/vue.js'

// new Vue表示建立一个dom所对应的抽象对象。
new Vue({
    // el属性定义了使用什么selector来获取到这个对象对应的dom，
    // 因为我们要对应的dom对象id为app，所以此处使用 #app 作为 selector
    el: '#app',
    data: {
        message: "Hello Vue.js"
    }
})
// 一旦vue将某个dom对象抽象为了一个js对象后，就会对其进行渲染，
// 我们之前写的 {{ message }} 就会被渲染为真正你想在页面上显示的文本。
// data属性定义了渲染这个dom对象是需要赋值的属性集合。我们定义message属性的值为"Hello Vue.js!"