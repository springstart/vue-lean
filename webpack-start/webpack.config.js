// webpack 的配置文件

// 输入配置
var path = require('path');

// 导出的详细配置
module.exports = {
    // 主要用到的模式有production和development
    // 开发时使用development
    mode: 'development',
    // 需要打包的 js 文件
    entry: './src/main.js',
    output: {
        // 输出路径
        path: path.resolve(__dirname, 'dist'),
        // 输出的文件名称
        filename: 'bundle.js'
    }
};