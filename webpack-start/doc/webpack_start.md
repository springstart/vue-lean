### webpack 初始使用

#### Hello Wabpack

1. 创建 `package.json` 文件, 作为 `webpack` 打包的配置(类似 `pom.xml`)
```json
// webpack 打包文件
{
  // 定义项目名称
  "name": "vue-learn-webpack",
  // 定义项目版本号
  "version": "1.0.0"
}
```
2. 在 `package.json` 文件同级目录执行命令, 添加 `webpack` 到项目中

```sh
npm i --save-dev webpack webpack-cli
```

3. 执行完成后, 会出现如下内容

   1. 生成 `node_modules` 目录(导入的组件), 相关依赖包会自动导入
   2. 生成 `package-lock.json` 文件(加载的组件详细内容)

4. 为什么使用 `webpack`

   1. 通过webpack就可以把多个js文件打包成一个js文件。
   2. 使用了webpack就可以使用import语句来导入别的js文件，这样做有两个好处
      * 不需要将公共库的js引用写到页面上来了，比如vue的引用就不需要写到index.html页面上了。
      * 你可以将你的所有js脚本可以按模块来分文件存放了

5. 抽离 js 到 `main.js` 文件中

6. 创建 `index.html` 并引入(只引用这一个 js 文件) `main.js`

7. 创建 `webpack` 的配置文件 `webpack.config.js` 内容如下

   ```js
   // webpack 的配置文件
   
   // 输入配置
   var path = require('path');
   
   // 导出的详细配置
   module.exports = {
       // 主要用到的模式有production和development
       // 开发时使用development
       mode: 'development',
       // 需要打包的 js 文件
       entry: './src/main.js',
       output: {
           // 输出路径
           path: path.resolve(__dirname, 'dist'),
           // 输出的文件名称
           filename: 'bundle.js'
       }
   };
   ```

   简单的讲解一下这个webpack.config.js

   * mode: 主要用到的模式有production和development。开发时使用development，所以我们现在用development，其实你就算用production对我们的例子也没什么影响。
   * entry: 我嫩之前不是说过使用了webpack之后就可以引用import来导入别的js文件么？那么你可以想象一下，你项目中的js可以构成一个引用树。这个引用树总要有一个树根的，这个树根是不会被任何js所引用的，所有引用最后都可以回溯到它。这里的entry就是webpack的js引用树树根文件
   * output：定义了打包后文件要存放的路径和文件名

8. 创建输出目录 `dist`

9. 执行命令进行打包

   ```sh
   npx webpack
   ```

10. 然后我们修改`index.html`页面上对`main.js`的引用为对`bundle.js`的引用

11. 接下来，我们来做更有意思的事情：将以下的`vue`的引用从页面上去除, 修改main.js脚本，在头部加上

    ```js
    import Vue from 'vue'
    ```

12. 使用 `npm` 安装 `vue`

    ```sh
    npm i --save vue
    ```

13. 运行完后，我们的 `package.json` 中自动新增了 `vue` 内容如下,node_modules文件夹下也增加了vue的包.

    ```json
    // 项目及项目中的组件配置
    {
      // 项目名称
      "name": "vue-learn-webpack",
      // 项目版本
      "version": "1.0.0",
      // 开发时的组件(jar) npm i --save-dev XXX
      "devDependencies": {
        "webpack": "^4.20.2",
        "webpack-cli": "^3.1.2"
      },
      // 运行时的组件 npm i --save XXX
      "dependencies": {
        "vue": "^2.5.16"
      }
    }
    ```

14. 我们再使用webpack来构建一次项目, 构建完毕了，此时我们打开dist/bundle.js看看。你会发现它把整个vue库都压缩了并写入了bundle.js文件。这样你的页面在打开时就可以一次性的把所有js库所有的依赖库都下载好了。

    我们再刷新一次页面，确保你还能看到Hello Vue.js!, 这是会发现什么都没有, 并且控制台会出现 

    ```sh
    [Vue warn]: You are using the runtime-only build of Vue where the template compiler is not available. Either pre-compile the templates into render functions, or use the compiler-included build.
    ```

    这是因为如果你直接写 `import Vue from 'vue'` 那么调用的是运行时的vue版本，这个版本不带模板解析包。我们之前在`index.html`页面上引用的也不是运行时版本。所以在这个例子中正确的做法应该是 

    ```js
    // 导入 vue 的 js 文件(引用的是 node_modules 下的 vue 文件)
    import Vue from 'vue/dist/vue.js'
    ```

    

#### 组件化

