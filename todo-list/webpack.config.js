// webpack 的配置文件

// 输入配置
var path = require('path');

// vue-loader是webpack的一个插件。所以我们在webpack.config.js中先要声明出这个插件
const {VueLoaderPlugin} = require('vue-loader');

// 注入自动插件脚本
const HtmlWebpackPlugin = require('html-webpack-plugin');



// 导出的详细配置
module.exports = {
    // 主要用到的模式有production和development
    // 开发时使用development
    mode: 'development',
    // 需要打包的 js 文件
    entry: './src/main.js',
    output: {
        // 输出路径
        path: path.resolve(__dirname, 'dist'),
        // 输出的文件名称
        filename: 'bundle.js'
    },
    // 最后告诉webpack哪些文件需要用这个插件来解析
    module: {
        // 定义打包规则
        // 加入 vue 加载器
        rules: [{
            // 匹配的文件
            test: /\.vue$/,
            // 加载器
            loader: 'vue-loader',
        }, {
            // 添加样式 加载器
            test: /\.css$/,
            use: [
                'style-loader',
                'css-loader'
            ]
        }]
    },
    // 然后使用它 (注入插件)
    plugins: [
        new VueLoaderPlugin(),
        // 以下是HtmlWebpackPlugin的配置
        new HtmlWebpackPlugin({
            template: 'index.html',
            filename: './index.html', // 输出文件【注意：这里的根路径是module.exports.output.path】
            hash: true
        })
    ],

    // 启动自动打包, 需要在编译时加上
    // 开启自动打包
    watch: true,
    // 自动打包的配置
    watchOptions: {
        aggregateTimeout: 3000, // 编译的超时时间，单位：毫秒
        poll: 30 // 扫描项目的间隔时间，单位：秒
    }
};